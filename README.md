So I forgot to bring my notebook home so here we are. I suppose it saves paper but is it more eco? I'm not sure. But anyway, all in one repo because there aren't that many notes to be made but we have learned some useful commands and I would like to have a place for them to live should I need to come back to them.

### 19/04/22

Worked with Martyn today to hurry things along a lil.

`curl i canhazip.com` for your IP address

It's good practice to download things e.g. from bitbucket as a zip file so that you dont import all the git history. It's also good practice to `git add <file_name>` rather than `git add .`.

`history` will bring up all previous commands- we used this to grab them and create a script.


### 20/04/22

`cat create.sh > destroy.sh` will copy everything from create and paste it into destroy. We used this to create a destroy script; just replaced 'apply' with detroy. However, the destroy script needs to be in the opposite order of the create script. An example of why is because virtual machines were created within a subnet and they need to be destroyed before the subnet itself.

